/
├── readme.txt............................................stručný popis obsahu média
├── src
│   ├── impl..............................................zdrojové kódy implementace
│   └── thesis.................................zdrojová forma práce ve formátu LATEX
└── text..................................................................text práce
    └── thesis.pdf.........................................text práce ve formátu PDF