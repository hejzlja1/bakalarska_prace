type ClientMeta = {
  [key: string]: {
    url: string;
  };
};

export const clientsUris: ClientMeta = {
  bohemia: {
    url: 'http://localhost:3020',
  },
};
