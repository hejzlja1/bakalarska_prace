import { gql } from "@apollo/client";


export const getResourceFieldsDocument = gql`
    query getResourceFields ($name: String!) {
  __type(name: $name) {
    name
    inputFields {
      name
      type {
        kind
        name
        ofType {
          kind
          name
        }
      }
    }
  }
}
    `;
