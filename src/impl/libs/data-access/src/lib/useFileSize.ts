import { IImage } from '@bistudio-cms/data';
import useSWR from 'swr';

export function useFileSize(file: IImage | undefined) {
  const blobFetcher = (url: string) => fetch(url).then((res) => res.blob());
  const { data, error } = useSWR(file ? file.url : null, blobFetcher);

  return {
    fileSize: data?.size,
    isLoading: !error && !data,
    isError: error,
  };
}
