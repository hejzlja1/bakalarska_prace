import { IImage } from '@bistudio-cms/data';
import { useParams } from 'react-router-dom';
import useSWR from 'swr';
import { clientsUris } from '../spaces';

export function useOneMedia() {
  const { spaceName, mediaId } = useParams();
  const clientURI = clientsUris[`${spaceName}`];
  const { data, error, mutate } = useSWR<IImage>(
    mediaId && clientURI ? `${clientURI.url}/media-library/${mediaId}` : null
  );

  return {
    file: data,
    isLoading: !error && !data,
    isError: error,
    mutate,
  };
}
