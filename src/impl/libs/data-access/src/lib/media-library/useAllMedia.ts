import { IPublicFile } from '@bistudio-cms/data';
import { useParams } from 'react-router-dom';
import useSWR from 'swr';
import { clientsUris } from '../spaces';

export function useAllMedia() {
  const { spaceName } = useParams();
  const clientURI = clientsUris[`${spaceName}`];
  const { data, error, mutate } = useSWR<IPublicFile[]>(
    clientURI ? `${clientURI.url}/media-library` : null
  );

  return {
    files: data,
    isLoading: !error && !data,
    isError: error,
    mutate,
  };
}
