import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { Status } from '@bistudio-cms/data';

@InputType({ isAbstract: true })
export class CreateBaseDTO {
  @Field()
  @IsNotEmpty()
  title!: string;

  @Field(() => Status)
  @IsNotEmpty()
  status!: Status;
}
