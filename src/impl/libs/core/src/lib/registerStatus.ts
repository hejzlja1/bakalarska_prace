import { registerEnumType } from '@nestjs/graphql';
import { Status } from '@bistudio-cms/data';

registerEnumType(Status, {
  name: 'Status',
});
