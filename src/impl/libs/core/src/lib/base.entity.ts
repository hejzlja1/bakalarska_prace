import { Status } from '@bistudio-cms/data';
import { Prop, Schema } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import './registerStatus';

@Schema({ timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })
export class BaseEntity extends Document {
  @Prop({ required: true })
  title!: string;

  @Prop({ required: true, type: String, enum: Status, default: Status.DRAFT })
  status!: Status;

  @Prop({ default: Date.now })
  created_at!: Date;

  @Prop({ default: Date.now })
  updated_at!: Date;
}
