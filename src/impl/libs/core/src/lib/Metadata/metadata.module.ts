import { Module } from '@nestjs/common';
import { EntitiesService } from './entities.service';
import { MetadataController } from './metadata.controller';

@Module({
  providers: [EntitiesService],
  controllers: [MetadataController],
  exports: [EntitiesService],
})
export class MetadataModule {}
