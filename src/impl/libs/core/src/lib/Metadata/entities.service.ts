import { Injectable } from '@nestjs/common';
import { BaseEntity } from '../base.entity';

@Injectable()
export class EntitiesService {
  private readonly entities: Array<typeof BaseEntity> = [];

  getAll() {
    return this.entities;
  }

  registerEntity(entity: typeof BaseEntity) {
    this.entities.push(entity);
  }
}
