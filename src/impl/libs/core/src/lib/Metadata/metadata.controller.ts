import { Controller, Get } from '@nestjs/common';
import { EntitiesService } from './entities.service';

@Controller('metadata')
export class MetadataController {
  constructor(private entitiesService: EntitiesService) {}

  @Get()
  findAll() {
    return this.entitiesService.getAll().map((item) => item.name);
  }
}
