import { FilterableField, IDField } from '@nestjs-query/query-graphql';
import { GraphQLISODateTime, ID, ObjectType } from '@nestjs/graphql';
import { Status } from '@bistudio-cms/data';

@ObjectType({ isAbstract: true })
export class BaseDTO {
  @IDField(() => ID)
  id!: string;

  @FilterableField()
  title!: string;

  @FilterableField(() => GraphQLISODateTime)
  created_at!: Date;

  @FilterableField(() => GraphQLISODateTime)
  updated_at!: Date;

  @FilterableField(() => Status)
  status!: Status;
}
