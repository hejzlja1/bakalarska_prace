import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { MediaLibraryModule } from '../mediaLibrary.module';
import { ImagesService } from '../Images.service';
import { INestApplication } from '@nestjs/common';

// TODO
describe('Images ', () => {
  let app: INestApplication;
  const imagesService = { findAll: () => ['test'] };

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [MediaLibraryModule],
    })
      .overrideProvider(ImagesService)
      .useValue(imagesService)
      .compile();

    app = moduleRef.createNestApplication();
    await app.init();
  });

  it(`/GET media-library`, () => {
    return request(app.getHttpServer()).get('/media-library').expect(200).expect({
      data: imagesService.findAll(),
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
