import { Test } from '@nestjs/testing';
import { MediaLibraryController } from '../mediaLibrary.controller';
import { ImagesService } from '../Images.service';
import { Image } from '../entities/Image.entity';
import { v4 as uuid } from 'uuid';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';

describe('Media Library Controller', () => {
  let mediaController: MediaLibraryController;
  let imageService: ImagesService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [MediaLibraryController],
      providers: [
        ImagesService,
        {
          provide: getModelToken(Image.name),
          useValue: Model,
        },
      ],
    }).compile();

    imageService = moduleRef.get<ImagesService>(ImagesService);
    mediaController = moduleRef.get<MediaLibraryController>(MediaLibraryController);
  });

  describe('findAll', () => {
    it('should return an array of Image', async () => {
      const result = ['test'];
      jest.spyOn(imageService, 'findAll').mockImplementation(() => result);

      expect(await mediaController.findAll()).toBe(result);
    });
  });
});
