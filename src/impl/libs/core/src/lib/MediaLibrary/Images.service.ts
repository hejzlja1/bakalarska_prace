import { Injectable, NotFoundException } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { Image, ImageDocument } from './entities/Image.entity';
import UpdateAltDto from './updateAlt.dto';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { FileTypes } from './entities/constants';

@Injectable()
export class ImagesService {
  constructor(@InjectModel(FileTypes.Image) private imageModel: Model<ImageDocument>) {}

  async findAll() {
    return this.imageModel.find();
  }

  async findOne(id: string) {
    const image = this.imageModel.findById(id);
    if (!image) {
      throw new NotFoundException();
    }
    return image;
  }

  async updateAlt(id: string, updateAltDto: UpdateAltDto) {
    const image = this.imageModel.findByIdAndUpdate(id, updateAltDto);
    if (!image) {
      throw new NotFoundException();
    }
    return image;
  }

  async uploadImageFile(file: Express.Multer.File): Promise<Image> {
    // TODO Implement actual file save
    // const s3 = new S3();
    // const uploadResult = await s3.upload({
    //   Bucket: this.configService.get('AWS_PUBLIC_BUCKET_NAME'),
    //   Body: dataBuffer,
    //   Key: `${uuid()}-${filename}`
    // })
    //   .promise();

    const key = `${uuid()}-${file.originalname}`;

    const newFile = await this.imageModel.create({
      key: key,
      url: 'https://picsum.photos/300/200', // TODO replace with actual public url of file
    });
    return newFile;
  }

  async deletePublicFile(id: string) {
    //await this.publicFilesRepository.findOne({ id }); // TODO delete file from Object Storage
    const result = await this.imageModel.findByIdAndDelete(id);
    if (!result) {
      throw new NotFoundException();
    }
  }
}
