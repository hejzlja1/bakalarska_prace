import { DynamicModule, Module } from '@nestjs/common';
import { ImagesService } from './Images.service';
import { ImageSchema } from './entities/Image.entity';
import { MediaLibraryController } from './mediaLibrary.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PublicFile, PublicFileSchemaFactory } from './entities/PublicFile.entity';
import { FileTypes } from './entities/constants';

@Module({})
export class MediaLibraryModule {
  static register(collectionPrefix: string): DynamicModule {
    return {
      module: MediaLibraryModule,
      imports: [
        MongooseModule.forFeatureAsync([
          {
            name: PublicFile.name,
            useFactory: PublicFileSchemaFactory,
            discriminators: [{ name: FileTypes.Image, schema: ImageSchema }],
            collection: `${collectionPrefix}_${PublicFile.name.toLowerCase()}s`,
          },
        ]),
      ],
      providers: [ImagesService],
      controllers: [MediaLibraryController],
    };
  }
}
