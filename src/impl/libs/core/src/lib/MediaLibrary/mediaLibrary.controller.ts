import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Multer } from 'multer';
import { ImagesService } from './Images.service';
import UpdateAltDto from './updateAlt.dto';

@Controller('media-library')
export class MediaLibraryController {
  constructor(private readonly imageService: ImagesService) {}

  @Get()
  async findAll() {
    return this.imageService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    return this.imageService.findOne(id);
  }

  @Patch(':id')
  async updateAlt(@Param('id') id: string, @Body() updateAltDto: UpdateAltDto) {
    return this.imageService.updateAlt(id, updateAltDto);
  }

  @Delete(':id')
  async remove(@Param('id') id: string) {
    return this.imageService.deletePublicFile(id);
  }

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: Express.Multer.File) {
    if (!file.originalname.match(/\.(jpe?g|png|webp)$/i)) {
      throw new BadRequestException({
        error: 'Only jpg, jpeg, png and webp Image files are supported.',
      });
    }
    return await this.imageService.uploadImageFile(file);
  }
}
