import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ImageDocument = Image & Document;

@Schema()
export class Image {
  kind!: string;
  url!: string;
  created_at!: Date;
  updated_at!: Date;

  @Prop({ default: '' })
  alt!: string;
}

export const ImageSchema = SchemaFactory.createForClass(Image);
