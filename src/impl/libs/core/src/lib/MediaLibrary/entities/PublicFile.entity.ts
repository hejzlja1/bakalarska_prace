import { IPublicFile } from '@bistudio-cms/data';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { FileTypes } from './constants';

@Schema({
  timestamps: { createdAt: 'created_at', updatedAt: 'updated_at ' },
  discriminatorKey: 'kind',
})
export class PublicFile extends Document implements IPublicFile {
  @Prop({
    type: String,
    required: true,
    enum: Object.values(FileTypes), // you can add more entities
  })
  kind!: string;

  @Prop({ required: true })
  url!: string;

  @Prop({ required: true })
  key!: string;

  @Prop({ default: Date.now })
  created_at!: Date;

  @Prop({ default: Date.now })
  updated_at!: Date;
}

export const PublicFileSchemaFactory = () => {
  const schema = SchemaFactory.createForClass(PublicFile);
  return schema;
};
