import { forwardRef } from 'react';
import clsx from 'clsx';

interface ButtonProps
  extends React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  children?: React.ReactNode;
  className?: string;
  variant?: 'primary' | 'danger';
  loading?: boolean;
}

export const Button = forwardRef<HTMLButtonElement, ButtonProps>(
  ({ children, className, variant = 'default', loading, ...rest }, ref) => {
    return (
      <button
        className={clsx(
          'items-center w-full border disabled:bg-opacity-50 disabled:border-opacity-0 disabled:cursor-not-allowed transition-colors duration-150 font-medium text-base px-4 py-2 focus:outline-none focus:ring-2 focus:ring-offset-2 inline-flex justify-center rounded-md shadow-sm sm:text-sm sm:w-auto',
          className,
          {
            'bg-white border-gray-300 text-gray-700 hover:bg-gray-100 focus:ring-green-600':
              variant === 'default',
            'border-green-700 bg-green-700 hover:bg-green-800 text-white focus:ring-green-600':
              variant === 'primary',
            'bg-white border-gray-300 hover:bg-red-700 hover:border-red-700 hover:text-white focus:ring-red-600':
              variant === 'danger',
          }
        )}
        ref={ref}
        {...rest}
      >
        {loading ? 'Loading...' : children}
      </button>
    );
  }
);

export default Button;
