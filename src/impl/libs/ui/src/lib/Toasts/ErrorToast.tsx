import toast from 'react-hot-toast';
import { toastOptions } from './ToastOptions';
import { ExclamationCircleIcon, XIcon } from '@heroicons/react/solid';

const ErrorToast = (title: string, message?: string) => {
  return toast.custom(
    (t) => (
      <div
        className={`${
          t.visible ? 'animate-enter' : 'animate-leave'
        } max-w-md w-full p-2 bg-white shadow-md rounded-md text-sm pointer-events-auto flex ring-1 ring-black ring-opacity-5`}
      >
        <div className="p-2">
          <ExclamationCircleIcon className="w-6 h-6 text-red-600" />
        </div>
        <div className="flex-1 w-0 p-2">
          <div className="flex flex-col items-start">
            <div className="flex-1 font-medium text-base">{title}</div>
            {message && <div className="text-gray-700 mt-1">{message}</div>}
          </div>
        </div>
        <div className="flex flex-col justify-start items-start">
          <button
            onClick={() => toast.dismiss(t.id)}
            className="p-2 rounded-md inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-green-600"
          >
            <XIcon className="h-5 w-5" />
          </button>
        </div>
      </div>
    ),
    toastOptions
  );
};

export default ErrorToast;
