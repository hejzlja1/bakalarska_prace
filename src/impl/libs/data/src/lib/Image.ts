import { IPublicFile } from './PublicFile';

export interface IImage extends IPublicFile {
  alt: string;
}
