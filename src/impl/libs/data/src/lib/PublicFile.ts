export interface IPublicFile {
  _id?: string;
  url: string;
  key: string;
  created_at: Date;
  updated_at: Date;
}
