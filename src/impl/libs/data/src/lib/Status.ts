export enum Status {
  DRAFT = 'draft',
  PUBLISHED = 'published',
}
