import React, { useCallback } from 'react';
import { BubbleMenu, Editor } from '@tiptap/react';
import clsx from 'clsx';
import { LinkIcon } from '@heroicons/react/outline';

type CustomBubbleMenuProps = {
  editor: Editor;
};

const CustomBubbleMenu: React.FC<CustomBubbleMenuProps> = ({ editor }) => {
  const setLink = useCallback(() => {
    if (!editor) return;
    const previousUrl = editor.getAttributes('link')['href'];
    const url = window.prompt('Paste link...', previousUrl);

    // cancelled
    if (url === null) {
      return;
    }

    // empty
    if (url === '') {
      editor.chain().focus().extendMarkRange('link').unsetLink().run();

      return;
    }

    // update link
    editor.chain().focus().extendMarkRange('link').setLink({ href: url }).run();
  }, [editor]);

  return (
    <BubbleMenu
      className={clsx('text-sm bg-white shadow-md border border-gray-300 rounded-md flex')}
      editor={editor}
      tippyOptions={{ duration: 100 }}
      shouldShow={({ editor }) => {
        const defaultConditions =
          editor && editor.state && editor.state.selection && editor.state.selection.content();
        const customConditions = !editor.isActive('image') && !editor.isActive('horizontalRule');
        return defaultConditions && customConditions
          ? editor.state.selection.content().size > 0
          : false;
      }}
    >
      <button
        title="Link"
        type="button"
        onClick={setLink}
        className={clsx('p-2 border-r border-gray-300 rounded-l-md flex items-center', {
          'bg-gray-900 text-white ': editor.isActive('link'),
          'bg-white': !editor.isActive('link'),
        })}
      >
        <LinkIcon className="h-4 w-4" />
      </button>
      <button
        title="Bold"
        type="button"
        onClick={() => editor.chain().focus().toggleBold().run()}
        className={clsx('p-2 border-r border-gray-300 font-semibold', {
          'bg-gray-900 text-white': editor.isActive('bold'),
          'bg-white': !editor.isActive('bold'),
        })}
      >
        B
      </button>
      <button
        title="Italicize"
        type="button"
        onClick={() => editor.chain().focus().toggleItalic().run()}
        className={clsx('p-2  border-r border-gray-300 italic', {
          'bg-gray-900 text-white': editor.isActive('italic'),
          'bg-white': !editor.isActive('italic'),
        })}
      >
        i
      </button>
      <button
        title="Underline"
        type="button"
        onClick={() => editor.chain().focus().toggleUnderline().run()}
        className={clsx('p-2  border-r border-gray-300 underline', {
          'bg-gray-900 text-white': editor.isActive('underline'),
          'bg-white': !editor.isActive('underline'),
        })}
      >
        U
      </button>
      <button
        title="Strike-through"
        type="button"
        onClick={() => editor.chain().focus().toggleStrike().run()}
        className={clsx('p-2 line-through border-r border-gray-300', {
          'bg-gray-900 text-white ': editor.isActive('strike'),
          'bg-white': !editor.isActive('strike'),
        })}
      >
        S
      </button>
      <button
        title="Mark as code"
        type="button"
        onClick={() => editor.chain().focus().toggleCode().run()}
        className={clsx('p-2 line-through rounded-r-md', {
          'bg-gray-900 text-white ': editor.isActive('code'),
          'bg-white': !editor.isActive('code'),
        })}
      >
        <svg
          viewBox="0 0 30 30"
          className="w-[15px] h-[15px] block fill-current flex-shrink-0"
          style={{ backfaceVisibility: 'hidden' }}
        >
          <path d="M11.625,4L0,15l11.625,11L13,24.563L2.906,15L13,5.438L11.625,4z M18.375,4L17,5.438L27.094,15L17,24.563L18.375,26L30,15L18.375,4z"></path>
        </svg>
      </button>
    </BubbleMenu>
  );
};

export default CustomBubbleMenu;
