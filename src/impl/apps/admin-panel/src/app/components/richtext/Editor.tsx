import { useEditor, EditorContent } from '@tiptap/react';
import StarterKit from '@tiptap/starter-kit';
// import Image from '@tiptap/extension-image';
import Link from '@tiptap/extension-link';
import CustomBubbleMenu from './CustomBubbleMenu';
import Underline from '@tiptap/extension-underline';
import Placeholder from '@tiptap/extension-placeholder';
import { IFrame, Commands } from './extensions';
import { FieldProps } from 'formik';
import ImageExtension from './extensions/components/Image/ImageExtension';

type EditorProps = FieldProps;

const TipTapEditor: React.FC<EditorProps> = ({ field, form }) => {
  const editor = useEditor({
    extensions: [
      ImageExtension,
      StarterKit.configure({
        heading: {
          levels: [1, 2, 3],
        },
        dropcursor: {
          width: 4,
          color: '#16a34a99',
        },
      }),
      Link.configure({
        openOnClick: false,
      }),
      Underline,
      IFrame,
      Placeholder.configure({
        emptyNodeClass:
          'before:text-gray-400 before:float-left before:content-[attr(data-placeholder)] before:h-0 before:pointer-events-none',
        placeholder: ({ node }) => {
          if (node.type.name === 'heading') {
            const levelMap: string[] = ['Heading 1', 'Heading 2', 'Heading 3'];
            return levelMap[node.attrs['level'] - 1];
          }

          return "Type '/' for commands";
        },
      }),
      Commands,
    ],
    editorProps: {
      attributes: {
        class:
          'prose prose-img:rounded-md p-8 max-w-none focus:outline-none focus:border-green-700 rounded-md min-h-[16rem]',
      },
    },
    content: field.value,
    onUpdate({ editor }) {
      form.setFieldValue(field.name, editor.getJSON());
    },
  });

  if (!editor) return null;

  return (
    <div className="border rounded-md bg-white">
      <EditorContent editor={editor} />
      <CustomBubbleMenu editor={editor} />
    </div>
  );
};

export default TipTapEditor;
