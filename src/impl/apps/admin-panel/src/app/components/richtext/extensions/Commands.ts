import { Editor, Extension, Range, ReactRenderer } from '@tiptap/react';
import Suggestion from '@tiptap/suggestion';
import tippy from 'tippy.js';
import CommandsList, { CommandItem } from './components/CommandsList';
import TextImage from '../../../../assets/images/text.png';
import Header1Image from '../../../../assets/images/header1.png';
import Header2Image from '../../../../assets/images/header2.png';
import Header3Image from '../../../../assets/images/header3.png';
import ImageImage from '../../../../assets/images/image.png';
import CodeImage from '../../../../assets/images/code.png';
import QuoteImage from '../../../../assets/images/quote.png';
import BulletedListImage from '../../../../assets/images/bulleted-list.png';
import NumberedListImage from '../../../../assets/images/numbered-list.png';
import DividerImage from '../../../../assets/images/divider.png';
import { includes } from 'lodash';

export type CommandType = { editor: Editor; range: Range; props: any };

const commands: CommandItem[] = [
  {
    title: 'Text',
    description: 'Just start writing with plain text.',
    image: TextImage,
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).setParagraph().run();
    },
  },
  {
    title: 'Heading 1',
    description: 'Big section heading.',
    image: Header1Image,
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).setHeading({ level: 1 }).run();
    },
  },
  {
    title: 'Heading 2',
    image: Header2Image,
    description: 'Medium section heading.',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).setHeading({ level: 2 }).run();
    },
  },
  {
    title: 'Heading 3',
    image: Header3Image,
    description: 'Small section heading.',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).setHeading({ level: 3 }).run();
    },
  },

  {
    title: 'Bulleted List',
    image: BulletedListImage,
    description: 'Create a simple bulleted list.',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).toggleBulletList().run();
    },
  },
  {
    title: 'Numbered List',
    image: NumberedListImage,
    description: 'Create a list with numbering.',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).toggleOrderedList().run();
    },
  },
  {
    title: 'Quote',
    image: QuoteImage,
    description: 'Capture a quote',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).toggleBlockquote().run();
    },
  },
  {
    title: 'Divider',
    image: DividerImage,
    description: 'Visually divide blocks.',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).setHorizontalRule().run();
    },
  },
  {
    title: 'Image',
    image: ImageImage,
    description: 'Upload or embed with a link.',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).setImage({ src: '' }).run();
    },
  },
  {
    title: 'Code',
    image: CodeImage,
    description: 'Capture a code snippet.',
    command: ({ editor, range }: CommandType) => {
      editor.chain().focus().deleteRange(range).toggleCodeBlock().run();
    },
  },
];

export default Extension.create({
  name: 'editor-commands',

  addProseMirrorPlugins() {
    return [
      Suggestion({
        char: '/',
        editor: this.editor,
        command: ({ editor, range, props }) => {
          props.command({ editor, range });
        },
        items: ({ query }) => {
          return commands
            .filter((item) => includes(item.title.toLowerCase(), query.toLowerCase()))
            .slice(0, 10);
        },
        render: () => {
          let component: any;
          let popup: any;

          return {
            onStart: (props) => {
              component = new ReactRenderer(CommandsList, {
                props,
                editor: props.editor,
              });

              popup = tippy('body', {
                getReferenceClientRect: props.clientRect,
                appendTo: () => document.body,
                content: component.element,
                showOnCreate: true,
                interactive: true,
                trigger: 'manual',
                placement: 'bottom-start',
              });
            },

            onUpdate(props) {
              component.updateProps(props);

              popup[0].setProps({
                getReferenceClientRect: props.clientRect,
              });
            },

            onKeyDown(props) {
              if (props.event.key === 'Escape') {
                popup[0].hide();

                return true;
              }

              return component.ref?.onKeyDown(props);
            },

            onExit() {
              popup[0].destroy();
              component.destroy();
            },
          };
        },
      }),
    ];
  },
});
