import { PhotographIcon } from '@heroicons/react/outline';
import { NodeViewProps, NodeViewWrapper } from '@tiptap/react';
import React, { useCallback, useState } from 'react';
import { isEmpty } from 'lodash';
import { Dialog } from '@headlessui/react';
import { Button, Modal } from '@bistudio-cms/ui';
import { useAllMedia } from '@bistudio-cms/data-access';
import { RadioGroup } from '@headlessui/react';
import { IImage } from '@bistudio-cms/data';
import { CheckIcon } from '@heroicons/react/solid';
import clsx from 'clsx';
import MediaEmptyState from '../../../../media-library/MediaEmptyState';

const ImageComponent: React.FC<NodeViewProps> = ({ node, updateAttributes }) => {
  const [open, setOpen] = useState<boolean>(false);
  const { files, isLoading, isError } = useAllMedia();
  const [selectedFile, setFile] = useState<IImage | null>(null);

  const handleClick = useCallback(() => {
    setFile(null);
    setOpen(true);
  }, []);

  const handleMediaSelect = useCallback(() => {
    if (!selectedFile) return;
    updateAttributes({
      src: selectedFile.url,
      alt: selectedFile.alt,
    });
    setOpen(false);
  }, [updateAttributes, selectedFile]);

  return (
    <NodeViewWrapper data-drag-handle className="mb-4">
      {isEmpty(node.attrs['src']) ? (
        <>
          <div
            onClick={handleClick}
            className="p-4 bg-gray-100 text-gray-500 text-sm cursor-pointer duration-200 hover:bg-gray-200 hover:text-gray-600 transition-colors rounded-md font-medium flex items-center gap-x-4"
          >
            <PhotographIcon className="w-6 h-6" />
            <span>Add an image</span>
          </div>
          <Modal open={open} setOpen={setOpen}>
            <div className="bg-white">
              <div className="flex items-center justify-between p-8">
                <Dialog.Title as="h3" className="text-2xl font-bold text-gray-900">
                  Media Library
                </Dialog.Title>
              </div>
              <div className="px-8 max-h-[calc(90vh-8rem)] pb-24 overflow-y-auto">
                {isError && JSON.stringify(isError, null, 2)}
                {isLoading && <span>Loading...</span>}
                {files && files.length > 0 ? (
                  <RadioGroup
                    className="grid grid-cols-4 gap-4"
                    value={selectedFile}
                    onChange={setFile}
                  >
                    {files.map((item) => (
                      <RadioGroup.Option className="focus:outline-none" value={item} key={item.key}>
                        {({ checked }) => (
                          <div>
                            <div className="relative rounded-md aspect-[3/2]">
                              <img className="rounded-md" src={item.url} alt="" />
                              {checked && (
                                <div
                                  className={clsx(
                                    'absolute inset-0 w-full h-full rounded-md flex-shrink-0 bg-black/30 flex justify-end text-green-700',
                                    {
                                      'ring-2 ring-offset-2 ring-green-600': checked,
                                    }
                                  )}
                                >
                                  <CheckIcon className="w-6 h-6 p-1 rounded-full bg-green-200 mr-2 mt-2" />
                                </div>
                              )}
                            </div>
                            <div title={item.key.slice(37)} className="truncate">
                              {item.key.slice(37)}
                            </div>
                          </div>
                        )}
                      </RadioGroup.Option>
                    ))}
                  </RadioGroup>
                ) : (
                  <MediaEmptyState />
                )}
              </div>
            </div>
            {selectedFile && (
              <div className="absolute bottom-0 left-0 right-0 bg-gray-50 px-6 py-3 flex justify-end">
                <Button type="button" onClick={() => setOpen(false)}>
                  Cancel
                </Button>
                <Button
                  type="button"
                  variant="primary"
                  className="sm:ml-3"
                  onClick={() => handleMediaSelect()}
                >
                  Add selected
                </Button>
              </div>
            )}
          </Modal>
        </>
      ) : (
        <img src={node.attrs['src']} alt={node.attrs['alt']} />
      )}
    </NodeViewWrapper>
  );
};

export default ImageComponent;
