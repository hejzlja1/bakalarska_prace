import Image from '@tiptap/extension-image';
import { ReactNodeViewRenderer } from '@tiptap/react';
import Component from './ImageComponent';

const CustomImage = Image.extend({
  addNodeView() {
    return ReactNodeViewRenderer(Component);
  },
});

export default CustomImage;
