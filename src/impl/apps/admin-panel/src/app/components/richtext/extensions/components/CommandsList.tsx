import clsx from 'clsx';
import { forwardRef, KeyboardEvent, useEffect, useImperativeHandle, useState } from 'react';
import type { CommandType } from '../Commands';

export type CommandItem = {
  title: string;
  description: string;
  image: string;
  command: ({ editor, range }: CommandType) => void;
};

type CommandsListProps = {
  items: CommandItem[];
  command: any;
};

type RefProps = {
  onKeyDown: (event: KeyboardEvent) => void;
};

const CommandsList = forwardRef<RefProps, CommandsListProps>(({ items, command }, ref) => {
  const [selectedIndex, setSelectedIndex] = useState<number>(0);

  const upHandler = () => {
    setSelectedIndex((selectedIndex + items.length - 1) % items.length);
  };

  const downHandler = () => {
    setSelectedIndex((selectedIndex + 1) % items.length);
  };

  const enterHandler = () => {
    selectItem(selectedIndex);
  };

  const selectItem = (index: number) => {
    const item = items[index];

    if (item) {
      command(item);
    }
  };

  useEffect(() => setSelectedIndex(0), [items]);

  useImperativeHandle(ref, () => ({
    onKeyDown: ({ event }: any) => {
      if (event.key === 'ArrowUp') {
        upHandler();
        return true;
      }

      if (event.key === 'ArrowDown') {
        downHandler();
        return true;
      }

      if (event.key === 'Enter') {
        enterHandler();
        return true;
      }

      return false;
    },
  }));

  return (
    <div className="flex flex-col gap-y-1 border p-1 rounded-md max-h-[40vh] min-w-[180px] overflow-y-auto bg-white shadow-lg">
      {items.length ? (
        items.map((item, index) => (
          <button
            className={clsx('flex rounded-md p-1 gap-x-2 items-center hover:bg-gray-100', {
              'bg-gray-100': index === selectedIndex,
            })}
            key={index}
            onClick={() => selectItem(index)}
          >
            <img src={item.image} alt="" width={46} height={46} className="rounded-md border" />
            <div className="flex items-start w-full flex-col gap-x-1 pr-2">
              <div className="text-sm text-gray-900">{item.title}</div>
              <div className="text-xs text-gray-500">{item.description}</div>
            </div>
          </button>
        ))
      ) : (
        <div className="text-sm p-2">No result</div>
      )}
    </div>
  );
});

export default CommandsList;
