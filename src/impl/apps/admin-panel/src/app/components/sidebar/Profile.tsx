import { LogoutIcon } from '@heroicons/react/solid';
import { Link } from 'react-router-dom';

const Profile = () => {
  return (
    <div className="flex justify-between border-t p-4">
      <div>
        <div className="text-sm">Tom Cook</div>
        <div className="text-xs text-gray-500">Editor</div>
      </div>
      <Link to="/">
        <button className="p-2 hover:bg-gray-200 rounded-md">
          <LogoutIcon className="h-6 w-6 text-gray-500" />
        </button>
      </Link>
    </div>
  );
};

export default Profile;
