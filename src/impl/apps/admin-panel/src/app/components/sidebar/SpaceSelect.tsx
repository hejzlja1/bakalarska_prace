import { capitalize } from 'lodash';
import { useParams, useNavigate } from 'react-router-dom';
import { Fragment, useState } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid';

type SpaceSelectProps = {
  spaces: string[];
};

const SpaceSelect: React.FC<SpaceSelectProps> = ({ spaces }) => {
  const { spaceName } = useParams();
  const [selected, setSelected] = useState(spaceName);
  const navigate = useNavigate();

  return (
    <div>
      <div className="uppercase text-gray-400 text-xs tracking-wide font-semibold mb-2">
        Workspace
      </div>
      <Listbox
        value={selected}
        onChange={(space) => {
          setSelected(space);
          navigate(`/${space}`);
        }}
      >
        <div className="relative mt-1">
          <Listbox.Button className="relative w-full py-2 pl-3 pr-10 text-left bg-white rounded-lg shadow-sm cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-green-300 focus-visible:ring-offset-2 focus-visible:border-green-500 sm:text-sm">
            <span className="block truncate">{capitalize(selected)}</span>
            <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
              <SelectorIcon className="w-5 h-5 text-gray-400" aria-hidden="true" />
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
              {spaces.map((space, spaceIdx) => (
                <Listbox.Option
                  key={spaceIdx}
                  className={({ active }) =>
                    `cursor-pointer select-none relative py-2 pl-10 pr-4 ${
                      active ? 'bg-gray-100' : 'text-gray-900'
                    }`
                  }
                  value={space}
                >
                  {({ selected }) => (
                    <>
                      <span
                        className={`block truncate ${selected ? 'font-medium' : 'font-normal'}`}
                      >
                        {capitalize(space)}
                      </span>
                      {selected ? (
                        <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-green-600">
                          <CheckIcon className="w-5 h-5" aria-hidden="true" />
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  );
};

export default SpaceSelect;
