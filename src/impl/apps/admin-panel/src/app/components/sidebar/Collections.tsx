import { toLower } from 'lodash';
import React from 'react';
import { Link } from 'react-router-dom';
import { fetcher } from '../../lib/fetcher';
import { ErrorToast } from '@bistudio-cms/ui';
import { useParams } from 'react-router-dom';
import { clientsUris } from '@bistudio-cms/data-access';
import useSWRImmutable from 'swr/immutable';

function useCollections() {
  const { spaceName } = useParams();
  const clientURI = clientsUris[`${spaceName}`];
  const { data, error } = useSWRImmutable(clientURI ? `${clientURI.url}/metadata` : null, fetcher);

  return {
    collections: data,
    isLoading: !error && !data,
    isError: error,
  };
}

const Collections = () => {
  const { collections, isLoading, isError } = useCollections();

  if (isLoading) return <div>Loading...</div>;
  if (isError) {
    ErrorToast(isError.name, isError.message);
    return null;
  }

  return (
    <div>
      <div className="uppercase text-gray-400 text-xs tracking-wide font-semibold mb-2">
        Collections
      </div>
      <nav>
        <ul className="flex flex-col gap-y-1">
          {collections &&
            collections.map((item: string) => (
              <li key={item}>
                <Link
                  className="py-2 px-2 w-full font-medium text-sm flex hover:bg-gray-200 rounded-md"
                  to={toLower(item)}
                >
                  {item}s
                </Link>
              </li>
            ))}
        </ul>
      </nav>
    </div>
  );
};

export default Collections;
