import React from 'react';
import Collections from './Collections';
import MediaLibraryLink from './MediaLibraryLink';
import Profile from './Profile';
import SpaceSelect from './SpaceSelect';

type SidebarProps = {
  spaces: string[];
};

const Sidebar: React.FC<SidebarProps> = ({ spaces }) => {
  return (
    <aside className="bg-gray-100 flex flex-col justify-between border-r">
      <div className="flex flex-col space-y-8 p-4">
        <SpaceSelect spaces={spaces} />
        <Collections />
        <hr />
        <MediaLibraryLink />
      </div>
      <Profile />
    </aside>
  );
};

export default Sidebar;
