import { PhotographIcon } from '@heroicons/react/outline';
import { Link } from 'react-router-dom';

const MediaLibraryLink = () => {
  return (
    <div>
      <Link
        className="inline-flex items-center py-2 px-2 w-full font-medium text-sm hover:bg-gray-200 rounded-md"
        to={'media-library'}
      >
        <PhotographIcon className="h-6 w-6 mr-2" />
        Media Library
      </Link>
    </div>
  );
};

export default MediaLibraryLink;
