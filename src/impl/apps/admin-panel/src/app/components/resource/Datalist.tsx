import { DocumentNode, useQuery } from '@apollo/client';
import React from 'react';
import { ResourceItem } from '../../lib/types/Resource';
import DatalistTable from './DatalistTable';

type DataListProps = {
  document: DocumentNode;
};

const DataList: React.FC<DataListProps> = ({ document }) => {
  const { data, loading, error } = useQuery(document, {
    variables: { paging: { first: 30 }, sorting: [{ field: 'created_at', direction: 'DESC' }] },
  });

  if (loading) return <div>Loading...</div>;
  if (error) return <div>{JSON.stringify(error, null, 2)}</div>;

  const resultArray: ResourceItem[] = (Object.values(data)[0] as any).edges as ResourceItem[];
  return <DatalistTable resultArray={resultArray} />;
};

export default DataList;
