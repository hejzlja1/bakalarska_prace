import { Button } from '@bistudio-cms/ui';
import { TrashIcon } from '@heroicons/react/outline';
import dayjs from 'dayjs';
import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { SpaceAPI } from '@bistudio-cms/data-access';
import { capitalize } from 'lodash';
import { useMutation } from '@apollo/client';
import clsx from 'clsx';

type MetaDataProps = {
  id?: string;
  created_at: string;
  updated_at: string;
};

const MetaData: React.FC<MetaDataProps> = ({ id, created_at, updated_at }) => {
  const { spaceName, resourceName, resourceId } = useParams();
  const navigate = useNavigate();

  const SpaceKey = `${capitalize(spaceName)}Documents`;
  const removeKey = `DeleteOne${capitalize(resourceName)}Document`;
  const removeMutation = (SpaceAPI as any)[SpaceKey][removeKey];
  const [removeResource, { loading, error }] = useMutation(removeMutation);

  const handleRemove = async () => {
    const ok = window.confirm('Are you sure you want to delete this item?');
    if (ok) {
      await removeResource({
        variables: {
          input: {
            id: resourceId,
          },
        },
        onCompleted: () => navigate(-1),
      });
    }
  };

  return (
    <div className="border-t bg-gray-50 p-8 text-sm">
      <ul className="space-y-4">
        <li>
          <div className="mb-2 block text-sm font-medium text-gray-700">Item ID:</div>
          <div>{id}</div>
        </li>
        <li>
          <div className="mb-2 block text-sm font-medium text-gray-700">Created:</div>
          <div>{dayjs(created_at).format('MMM D, YYYY h:mm A')}</div>
        </li>
        <li>
          <div className="mb-2 block text-sm font-medium text-gray-700">Last edited:</div>
          <div>{dayjs(updated_at).format('MMM D, YYYY h:mm A')}</div>
        </li>
      </ul>
      <Button
        onClick={handleRemove}
        className={clsx('mt-8')}
        disabled={loading}
        variant="danger"
        type="button"
      >
        <TrashIcon className="w-4 h-4 mr-1" />
        <span>Delete</span>
      </Button>
      {error && <pre>{JSON.stringify(error, null, 2)}</pre>}
    </div>
  );
};

export default MetaData;
