import React from 'react';
import { CellProps, useTable } from 'react-table';
import { ResourceItem, ResourceItemTable } from '../../lib/types/Resource';
import type { Column } from 'react-table';
import { useNavigate, useParams } from 'react-router-dom';
import type { Row } from 'react-table';
import dayjs from 'dayjs';
import { useCheckWithIdResource } from '../../lib/checkWithId';
import { StatusFrontend } from '../../pages/resources/edit';
import { CheckIcon, PencilIcon } from '@heroicons/react/outline';
import clsx from 'clsx';
import { ChevronRightIcon } from '@heroicons/react/solid';

const StatusCell: React.FC<CellProps<any>> = ({ value }) => {
  if (value === StatusFrontend.DRAFT) {
    return (
      <div className="inline-flex items-center text-amber-500">
        <PencilIcon className="h-4 w-4 mr-2" />
        <span>Draft</span>
      </div>
    );
  } else if (value === StatusFrontend.PUBLISHED) {
    return (
      <div className="inline-flex items-center text-green-600">
        <CheckIcon className="h-4 w-4 mr-2" />
        <span>Published</span>
      </div>
    );
  }

  return null;
};

const TitleCell: React.FC<CellProps<any>> = ({
  cell: { value },
  row: {
    original: { id },
  },
}) => {
  const { resourceId } = useParams();

  return (
    <div className="inline-flex items-center justify-between w-full">
      <span>{value}</span>
      {`${resourceId}` === id && <ChevronRightIcon className="h-5 w-5 text-gray-400 -mr-4" />}
    </div>
  );
};

type DatalistTable = {
  resultArray: ResourceItem[];
};

const DatalistTable: React.FC<DatalistTable> = ({ resultArray }) => {
  const navigate = useNavigate();
  const { resourceId } = useParams();
  const isDetail = useCheckWithIdResource();
  const handleRowClick = (row: Row<ResourceItemTable>) => {
    navigate(`${row.original.id}`);
  };
  const data = React.useMemo(
    () =>
      resultArray.map(({ node: { id, title, status, created_at, updated_at } }) => {
        return {
          id,
          title,
          status,
          created_at: dayjs(created_at).format('MMM D, YYYY h:mm A'),
          updated_at: dayjs(updated_at).format('MMM D, YYYY h:mm A'),
        };
      }),
    [resultArray]
  );

  const columns = React.useMemo<Column<ResourceItemTable>[]>(
    () => [
      {
        Header: 'Title',
        accessor: 'title', // accessor is the "key" in the data
        Cell: TitleCell,
      },
      {
        Header: 'Status',
        accessor: 'status',
        Cell: StatusCell,
      },
      {
        Header: 'Created At',
        accessor: 'created_at',
      },
      {
        Header: 'Modified At',
        accessor: 'updated_at',
      },
    ],
    []
  );

  const tableInstance = useTable({
    columns,
    data,
    initialState: { hiddenColumns: isDetail ? ['status', 'updated_at', 'created_at'] : [] },
  });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = tableInstance;

  return (
    <table className="min-w-full text-sm" {...getTableProps()}>
      <thead className="bg-gray-50 sticky top-0">
        {
          // Loop over the header rows
          headerGroups.map((headerGroup) => (
            // Apply the header row props
            <tr {...headerGroup.getHeaderGroupProps()}>
              {
                // Loop over the headers in each row
                headerGroup.headers.map((column) => (
                  // Apply the header cell props
                  <th
                    scope="col"
                    className="px-6 py-4 text-left text-xs font-medium text-gray-500 uppercase tracking-wider after:border-b after:border-gray-200 after:absolute after:w-full after:bottom-0 after:left-0"
                    {...column.getHeaderProps()}
                  >
                    {
                      // Render the header
                      column.render('Header')
                    }
                  </th>
                ))
              }
            </tr>
          ))
        }
      </thead>
      {/* <pre>{JSON.stringify(data, null, 2)}</pre> */}
      <tbody
        className="bg-white last:!border-b last:!border-gray-200 divide-y divide-gray-200"
        {...getTableBodyProps()}
      >
        {
          // Loop over the table rows
          rows.map((row) => {
            //console.log(`${resourceId}` === row.original.id.toString());
            // Prepare the row for display
            prepareRow(row);
            return (
              // Apply the row props
              <tr
                onClick={() => handleRowClick(row)}
                className={clsx('hover:bg-gray-50 cursor-pointer', {
                  'bg-gray-50': `${resourceId}` === row.original.id,
                })}
                {...row.getRowProps()}
              >
                {
                  // Loop over the rows cells
                  row.cells.map((cell) => {
                    // Apply the cell props
                    return (
                      <td className={clsx('px-6 py-4 whitespace-nowrap')} {...cell.getCellProps()}>
                        {
                          // Render the cell contents
                          cell.render('Cell')
                        }
                      </td>
                    );
                  })
                }
              </tr>
            );
          })
        }
      </tbody>
    </table>
  );
};

export default DatalistTable;
