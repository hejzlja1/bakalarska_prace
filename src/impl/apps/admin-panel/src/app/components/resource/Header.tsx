import { Button } from '@bistudio-cms/ui';
import { SearchIcon } from '@heroicons/react/outline';
import { PlusIcon } from '@heroicons/react/solid';
import clsx from 'clsx';
import { capitalize } from 'lodash';
import { Link, useParams } from 'react-router-dom';
import { useCheckWithIdResource } from '../../lib/checkWithId';

const SearchInput = () => {
  const { resourceName } = useParams();
  return (
    <div className="relative text-gray-400 focus-within:text-gray-500">
      <span className="absolute inset-y-0 left-0 flex items-center pl-2">
        <SearchIcon className="w-5 h-5" />
      </span>
      <input
        type="search"
        name="search"
        className="placeholder:text-gray-400 focus-within:placeholder:text-gray-500 border border-gray-300 py-2 text-sm rounded-md pl-8 pr-2 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-green-600"
        placeholder={`Search ${resourceName}s...`}
        autoComplete="off"
      />
    </div>
  );
};

const Header = () => {
  const { resourceName } = useParams();
  const isDetail = useCheckWithIdResource();

  return (
    <div className="flex justify-between items-center px-8 border-b h-24">
      <h1 className="font-bold text-2xl">{capitalize(resourceName)}s</h1>
      <div
        className={clsx('flex gap-4', {
          hidden: isDetail,
        })}
      >
        {/* <SearchInput /> */}
        {/* <Button>Filter</Button> */}
        {/* <Button>Bulk Select...</Button> */}
        <Link to="create">
          <Button variant="primary">
            <PlusIcon className="w-4 h-4 text-white mr-1" />
            New {resourceName}
          </Button>
        </Link>
      </div>
    </div>
  );
};

export default Header;
