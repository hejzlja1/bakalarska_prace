import { CloudUploadIcon } from '@heroicons/react/solid';
import clsx from 'clsx';
import { useCallback, useEffect, useState } from 'react';
import { isFile } from '../../lib/isFile';

type DropzoneProps = {
  handleUpload: (filelist: FileList) => Promise<void>;
};

const Dropzone: React.FC<DropzoneProps> = ({ handleUpload }) => {
  const [isVisible, setVisible] = useState<boolean>(false);
  const [lastTarget, setLastTarget] = useState(null);

  const handleDragEnter = useCallback((e) => {
    if (isFile(e)) {
      setLastTarget(e.target);
      setVisible(true);
    }
  }, []);

  const handleDragOver = useCallback((e) => {
    e.preventDefault();
  }, []);

  const handleDragLeave = useCallback(
    (e) => {
      if (e.target === lastTarget || e.target === document) {
        setVisible(false);
      }
    },
    [lastTarget]
  );

  const handleDrop = useCallback(
    (e) => {
      e.preventDefault();
      handleUpload(e.dataTransfer.files);
      setVisible(false);
    },
    [handleUpload, setVisible]
  );

  useEffect(() => {
    window.addEventListener('dragenter', handleDragEnter);
    window.addEventListener('dragover', handleDragOver);
    window.addEventListener('dragleave', handleDragLeave);
    window.addEventListener('drop', handleDrop);

    return () => {
      window.removeEventListener('dragenter', handleDragEnter);
      window.removeEventListener('dragover', handleDragOver);
      window.removeEventListener('dragleave', handleDragLeave);
      window.removeEventListener('drop', handleDrop);
    };
  }, [handleDragEnter, handleDragOver, handleDragLeave, handleDrop]);

  return (
    <div
      className={clsx(
        'fixed inset-0 z-50 h-full w-full text-white font-bold text-2xl bg-black/80 transition-all duration-200 grid place-items-center',
        {
          'invisible opacity-0': !isVisible,
          'visible opacity-100': isVisible,
        }
      )}
    >
      <div className="flex flex-col gap-y-4 items-center">
        <CloudUploadIcon className="w-8 h-8" />
        <div>Add to Media Library</div>
      </div>
    </div>
  );
};

export default Dropzone;
