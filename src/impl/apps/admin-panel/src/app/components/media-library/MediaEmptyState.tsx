import { PhotographIcon } from '@heroicons/react/outline';

const MediaEmptyState = () => {
  return (
    <div className="p-24 border-dashed border-2 border-gray-300 rounded-md flex flex-col items-center text-center gap-y-2 text-sm">
      <PhotographIcon className="h-10 w-10 text-gray-500" />
      <h2 className="font-medium text-lg">No media</h2>
      <p className="text-gray-600">Get started by adding new images, videos and other media.</p>
    </div>
  );
};

export default MediaEmptyState;
