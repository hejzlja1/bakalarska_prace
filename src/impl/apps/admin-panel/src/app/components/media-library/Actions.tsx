import { IPublicFile } from '@bistudio-cms/data';
import { Button } from '@bistudio-cms/ui';
import { DownloadIcon, TrashIcon } from '@heroicons/react/outline';
import React from 'react';

type ActionsProps = {
  file: IPublicFile;
  handleRemove: () => void;
  loading: boolean;
};

const Actions: React.FC<ActionsProps> = ({ file, handleRemove, loading }) => {
  return (
    <div className="flex gap-x-2 mt-8">
      <a
        className="flex-1 items-center bg-white border-gray-300 text-gray-700 hover:bg-gray-100 focus:ring-green-600 w-full border disabled:bg-opacity-50 disabled:border-opacity-0 disabled:cursor-not-allowed transition-colors duration-150 font-medium text-base px-4 py-2 focus:outline-none focus:ring-2 focus:ring-offset-2 inline-flex justify-center rounded-md shadow-sm sm:text-sm sm:w-auto"
        href={file.url}
        download={file.key.slice(37)}
      >
        <DownloadIcon className="w-4 h-4 mr-1" />
        <span>Download</span>
      </a>
      <Button
        className="flex-1"
        onClick={handleRemove}
        disabled={loading}
        variant="danger"
        type="button"
      >
        <TrashIcon className="w-4 h-4 mr-1" />
        <span>Delete</span>
      </Button>
    </div>
  );
};

export default Actions;
