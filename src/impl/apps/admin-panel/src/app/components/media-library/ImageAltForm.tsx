import { IImage } from '@bistudio-cms/data';
import { Spinner } from '@bistudio-cms/ui';
import { CheckCircleIcon } from '@heroicons/react/solid';
import axios from 'axios';
import { Field, Form, Formik } from 'formik';
import { isEmpty } from 'lodash';
import { useRef, useState } from 'react';
import { useParams } from 'react-router-dom';
import { KeyedMutator } from 'swr';
import { clientsUris } from '@bistudio-cms/data-access';
import { getChangedValues } from '../../pages/resources/edit';

type ImageAltFormProps = {
  alt: string;
  mutate: KeyedMutator<IImage>;
};

type AltInitialValues = {
  alt: string;
};

const ImageAltForm: React.FC<ImageAltFormProps> = ({ alt, mutate }) => {
  const [isSuccess, setSuccess] = useState(false);
  const { spaceName, mediaId } = useParams();
  const timeout = useRef<NodeJS.Timeout | undefined>(undefined);

  const initialValues: AltInitialValues = { alt };

  return (
    <Formik
      onSubmit={(values, { setSubmitting }) => {
        if (!spaceName || !mediaId) return;
        const clientURI = clientsUris[spaceName];
        const fullUri = `${clientURI.url}/media-library/${mediaId}`;
        setSubmitting(true);
        setSuccess(false);
        axios
          .patch(fullUri, { alt: values.alt })
          .then(() => {
            setSuccess(true);
            mutate();
          })
          .catch((e) => console.log(e))
          .finally(() => {
            setSubmitting(false);
          });
      }}
      initialValues={initialValues}
      enableReinitialize={true}
    >
      {({ submitForm, isSubmitting, values, initialValues }) => (
        <Form
          onFocus={() => setSuccess(false)}
          onBlur={() => {
            if (isEmpty(getChangedValues(values, initialValues))) return;
            if (timeout.current) clearTimeout(timeout.current);
            timeout.current = setTimeout(() => {
              submitForm();
            }, 700);
          }}
          className="mt-4 text-gray-500 font-medium flex flex-col gap-y-1"
        >
          <label className="text-sm inline-flex justify-between items-center">
            <span>Alt</span>
            {isSubmitting && <Spinner className="!text-black !ml-1 !w-4 !h-4 !mr-0" />}
            {isSuccess && <CheckCircleIcon className="h-4 w-4 text-green-600 ml-1" />}
          </label>
          <Field id="alt" as="textarea" name="alt" />
          {values.alt === '' && (
            <span className="text-xs mt-1 font-normal">
              Images without Alt attribute are considered "Decorative" and don't convey meaning.
            </span>
          )}
        </Form>
      )}
    </Formik>
  );
};

export default ImageAltForm;
