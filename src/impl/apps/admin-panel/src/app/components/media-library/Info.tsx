import { IPublicFile } from '@bistudio-cms/data';
import dayjs from 'dayjs';
import { MutableRefObject } from 'react';

type InfoProps = {
  file: IPublicFile;
  imageRef: MutableRefObject<HTMLImageElement | null>;
};

const Info: React.FC<InfoProps> = ({ file, imageRef }) => {
  return (
    <div className="mt-6">
      <div className="font-medium">Information</div>
      <hr className="mt-2" />
      <dl>
        <div className="flex justify-between text-sm font-medium border-b py-4">
          <dt className="text-gray-500">Uploaded By</dt>
          <dd>Jan Hejzlar</dd> {/* TODO replace with actual user */}
        </div>
        <div className="flex justify-between text-sm font-medium border-b py-4">
          <dt className="text-gray-500">Created</dt>
          <dd>{dayjs(file.created_at).format('MMMM D, YYYY')}</dd>
        </div>
        <div className="flex justify-between text-sm font-medium border-b py-4">
          <dt className="text-gray-500">Last Modified</dt>
          <dd>{dayjs(file.updated_at).format('MMMM D, YYYY')}</dd>
        </div>
        <div className="flex justify-between text-sm font-medium border-b py-4">
          <dt className="text-gray-500">Dimensions</dt>
          {imageRef.current && (
            <dd>
              {imageRef.current.naturalHeight} x {imageRef.current.naturalWidth}
            </dd>
          )}
        </div>
      </dl>
    </div>
  );
};

export default Info;
