import useObjectURL from 'use-object-url';

type LocalImageProsp = {
  file: File;
};

const LocalImage: React.FC<LocalImageProsp> = ({ file }) => {
  const objectURL = useObjectURL(file);

  if (!objectURL) return null;

  return (
    <img
      className="mb-2 aspect-[3/2] object-cover object-center rounded-lg group-focus-within:ring-2 group-focus-within:ring-offset-2 group-focus-within:ring-green-600"
      src={objectURL}
      alt=""
    />
  );
};

export default LocalImage;
