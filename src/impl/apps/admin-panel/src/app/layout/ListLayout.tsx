import clsx from 'clsx';
import { Outlet } from 'react-router-dom';
import { useCheckWithIdResource } from '../lib/checkWithId';
import List from '../pages/resources/list';

const ListLayout = () => {
  const isDetail = useCheckWithIdResource();
  return (
    <div
      className={clsx('grid', {
        'grid-cols-[1fr,2.5fr]': isDetail,
      })}
    >
      <List />
      <Outlet />
    </div>
  );
};

export default ListLayout;
