import { Outlet, useParams } from 'react-router-dom';
import { ApolloProvider } from '@apollo/client';
import getClient from '../lib/apollo-client';
import Sidebar from '../components/sidebar/Sidebar';
import { clientsUris } from '@bistudio-cms/data-access';

const SpaceDetailLayout = () => {
  const { spaceName } = useParams();
  if (!spaceName) return null;

  return (
    <ApolloProvider client={getClient(spaceName)}>
      <div className="grid grid-flow-col h-screen grid-cols-[minmax(250px,max-content),1fr]">
        <Sidebar spaces={Object.keys(clientsUris)} />
        <main className="bg-white">
          <Outlet />
        </main>
      </div>
    </ApolloProvider>
  );
};

export default SpaceDetailLayout;
