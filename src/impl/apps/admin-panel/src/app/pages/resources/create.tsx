import { useNavigate, useParams } from 'react-router-dom';
import { getResourceFieldsDocument, SpaceAPI } from '@bistudio-cms/data-access';
import { useQuery } from '@apollo/client';
import { capitalize } from 'lodash';
import { Field, Form, Formik } from 'formik';
import { useMutation } from '@apollo/client';
import { ArrowLeftIcon } from '@heroicons/react/outline';
import * as Yup from 'yup';
import { StatusFrontend, SubmitFormButton } from './edit';
import TipTapEditor from '../../components/richtext/Editor';
import React from 'react';

export const getFieldType = (field: any) => {
  return field.type.kind === 'NON_NULL' ? field.type.ofType.name : field.type.name;
};

export const excludedFieldTypes = ['Status'];

export const isFieldRequired = (field: any) => field.type.kind === 'NON_NULL';

const initialValuesMap: { [key: string]: any } = {
  String: '',
  Int: 0,
  Float: 0,
  DateTime: '',
  Boolean: false,
  Status: StatusFrontend.DRAFT,
  JSONObject: null,
};

export const inputTypeMap: { [key: string]: React.ReactElement } = {
  String: <Field type="text" />,
  Int: <Field type="number" />,
  Float: <Field type="number" />,
  DateTime: <Field type="date" />,
  Boolean: <Field type="checkbox" />,
  JSONObject: <Field component={TipTapEditor} />,
};

export const validationYupMap: { [key: string]: Yup.BaseSchema } = {
  String: Yup.string(),
  Int: Yup.number(),
  Float: Yup.number(),
  DateTime: Yup.date(),
  Boolean: Yup.bool(),
  Status: Yup.mixed<StatusFrontend>().oneOf(Object.values(StatusFrontend)),
};

export const composeFieldValidation = (field: any) => {
  const fieldType = getFieldType(field);
  let schema = validationYupMap[fieldType];
  if (isFieldRequired(field)) {
    schema = schema.required('Field is required.');
  } else {
    schema = schema.nullable();
  }
  return schema;
};

const Create = () => {
  const { spaceName, resourceName } = useParams();
  const navigate = useNavigate();

  const { data, loading, error } = useQuery(getResourceFieldsDocument, {
    variables: {
      name: `Create${capitalize(resourceName)}DTO`,
    },
  });

  const SpaceKey = `${capitalize(spaceName)}Documents`;
  const ResourceKey = `CreateOne${capitalize(resourceName)}Document`;
  const createMutation = (SpaceAPI as any)[SpaceKey][ResourceKey];
  const [createResource, { loading: mutLoading, error: mutError }] = useMutation(createMutation);

  if (loading) return <div>Loading...</div>;
  if (error) return <pre>{JSON.stringify(error, null, 2)}</pre>;

  const inputFields = data.__type.inputFields;

  const initialValues: any = {};
  const validationSchema: any = {};

  inputFields.forEach((field: any) => {
    const fieldType = getFieldType(field);
    initialValues[field.name] = initialValuesMap[fieldType];
    if (validationYupMap[fieldType]) {
      validationSchema[field.name] = composeFieldValidation(field);
    }
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={Yup.object().shape(validationSchema)}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          setSubmitting(true);
          await createResource({
            variables: { input: { [`${resourceName}`]: values } },
            onCompleted: () => navigate(-1),
          }).finally(() => {
            resetForm();
            setSubmitting(false);
          });
        }}
      >
        {({ errors, touched }) => (
          <Form className="flex flex-col items-start">
            <div className="flex items-center justify-between w-full px-8 h-24 border-b">
              <div className="flex items-center gap-x-4">
                <button
                  className="p-2 border shadow-sm rounded-md hover:bg-gray-50"
                  type="button"
                  onClick={() => navigate(-1)}
                >
                  <ArrowLeftIcon className="w-5 h-5 text-gray-400" />
                </button>
                <h2 className="font-semibold text-2xl">New {resourceName}</h2>
              </div>
              <SubmitFormButton />
            </div>
            <div className="p-8 w-full space-y-8 h-[calc(100vh-6rem)] overflow-auto">
              {/* {mutError && <pre className="text-sm p-4">{JSON.stringify(mutError, null, 2)}</pre>} */}
              {inputFields.map((field: any) => {
                const fieldType = getFieldType(field);
                const inputType: React.ReactElement = inputTypeMap[fieldType];
                if (excludedFieldTypes.includes(fieldType)) {
                  return null; // skip excluded fields
                }

                const fieldWithProps = React.cloneElement(inputType, {
                  className: 'border rounded-md px-3 py-2',
                  name: field.name,
                });

                return (
                  <div key={field.name} className="flex flex-col">
                    <label
                      className="mb-2 block text-sm font-medium text-gray-700"
                      htmlFor={field.name}
                    >
                      {capitalize(field.name)}
                      {isFieldRequired(field) && <span className="text-red-500">&nbsp;*</span>}
                    </label>
                    {fieldWithProps}
                    {touched[field.name] && errors[field.name] && (
                      <div className="mt-2 text-sm text-red-500">{errors[field.name]}</div>
                    )}
                  </div>
                );
              })}
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Create;
