import { capitalize, upperCase } from 'lodash';
import { SpaceAPI } from '@bistudio-cms/data-access';
import { useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { ResourceItem } from '../../lib/types/Resource';

const ResourceDetail = () => {
  const { resourceName, resourceId } = useParams();

  const key = `${capitalize(resourceName)}Document`;
  const document = (SpaceAPI as any)[key];
  const { data, loading, error } = useQuery(document, {
    variables: {
      id: +(resourceId as string),
    },
  });

  if (loading) return <div>Loading...</div>;
  if (error) return <div>{JSON.stringify(error, null, 2)}</div>;

  const result: ResourceItem = Object.values(data)[0] as ResourceItem;
  const renderResult = [];
  for (const [property, value] of Object.entries(result)) {
    renderResult.push(
      <div key={property} className="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
        <dt className="text-sm font-medium text-gray-500">{upperCase(property)}</dt>
        <dd className="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">{value}</dd>
      </div>
    );
  }
  return (
    <div className="bg-white shadow overflow-hidden sm:rounded-lg">
      <div className="px-4 py-5 sm:px-6">
        {/* <h3 className="text-lg leading-6 font-medium text-gray-900">{result.}</h3> */}
        <p className="mt-1 max-w-2xl text-sm text-gray-500">Description</p>
      </div>
      <div className="border-t border-gray-200">
        <dl>{renderResult}</dl>
      </div>
    </div>
  );
};

export default ResourceDetail;
