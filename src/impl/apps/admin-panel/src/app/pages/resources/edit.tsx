import { useMutation, useQuery } from '@apollo/client';
import { getResourceFieldsDocument, SpaceAPI } from '@bistudio-cms/data-access';
import { capitalize, isEqual, isEmpty } from 'lodash';
import { useNavigate, useParams } from 'react-router-dom';
import { useFormikContext, Form, Formik } from 'formik';
import {
  composeFieldValidation,
  excludedFieldTypes,
  getFieldType,
  inputTypeMap,
  validationYupMap,
} from './create';
import dayjs from 'dayjs';
import { ArrowLeftIcon, PencilIcon, CheckIcon } from '@heroicons/react/outline';
import { ChevronDownIcon } from '@heroicons/react/solid';
import * as Yup from 'yup';
import { isFieldRequired } from './create';
import { Menu, Transition } from '@headlessui/react';
import React, { Fragment } from 'react';
import clsx from 'clsx';
import MetaData from '../../components/resource/MetaData';
import { Spinner } from '@bistudio-cms/ui';
import { useBeforeunload } from 'react-beforeunload';
import { usePrompt } from '../../lib/useBlocker';

export enum StatusFrontend {
  DRAFT = 'DRAFT',
  PUBLISHED = 'PUBLISHED',
}

export const getChangedValues = (
  values: { [key: string]: any },
  initialValues: { [key: string]: any }
) => {
  return Object.entries(values).reduce((acc: { [key: string]: any }, [key, value]) => {
    if (!isEqual(initialValues[key], value)) {
      acc[key] = value;
    }

    return acc;
  }, {});
};

const SplitButton: React.FC = () => {
  const { initialValues, submitForm, setFieldValue, validateForm } = useFormikContext();
  const isPublished = (initialValues as any).status === StatusFrontend.PUBLISHED;
  const isDraft = (initialValues as any).status === StatusFrontend.DRAFT;

  const setStatusAndSubmit = async (
    e: React.MouseEvent<HTMLButtonElement>,
    status: StatusFrontend
  ) => {
    validateForm().then((errors) => {
      if (isEmpty(errors)) {
        setFieldValue('status', status);
      }
      submitForm();
    });
  };

  return (
    <Menu as="div" className="relative inline-block text-left">
      <Menu.Button
        className={clsx(
          'inline-flex justify-center w-full p-2 text-sm font-medium text-white rounded-r-md focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2',
          {
            'bg-green-700 focus-visible:ring-green-700':
              (initialValues as any).status === StatusFrontend.PUBLISHED,
            'bg-amber-500 focus-visible:ring-amber-500':
              (initialValues as any).status === StatusFrontend.DRAFT,
          }
        )}
      >
        <ChevronDownIcon className="w-5 h-5 " aria-hidden="true" />
      </Menu.Button>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <Menu.Items className="absolute right-0 z-10 w-64 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
          <Menu.Item disabled={isPublished}>
            {({ active, disabled }) => (
              <button
                disabled={disabled}
                onClick={(e) => setStatusAndSubmit(e, StatusFrontend.PUBLISHED)}
                type="button"
                className={clsx(
                  'group flex flex-col items-start rounded-t-md disabled:cursor-not-allowed disabled:opacity-50 w-full p-4 text-sm',
                  {
                    'bg-green-700 text-white': active,
                    'text-gray-900': !active,
                  }
                )}
              >
                <div className="flex justify-between font-medium items-center w-full">
                  <span>Publish</span>
                  <CheckIcon className="w-6 h-6 mr-2" aria-hidden="true" />
                </div>
                <p
                  className={clsx('text-left mt-2', {
                    'text-white': active,
                    'text-gray-600': !active,
                  })}
                >
                  This content will be publicly visible.
                </p>
              </button>
            )}
          </Menu.Item>
          <Menu.Item disabled={isDraft}>
            {({ active, disabled }) => (
              <button
                onClick={(e) => setStatusAndSubmit(e, StatusFrontend.DRAFT)}
                type="button"
                disabled={disabled}
                className={clsx(
                  'group flex flex-col items-start disabled:cursor-not-allowed disabled:opacity-50 rounded-b-md w-full p-4 text-sm',
                  {
                    'bg-amber-500 text-white': active,
                    'text-gray-900': !active,
                  }
                )}
              >
                <div className="flex justify-between font-medium items-center w-full">
                  <span>Save as draft</span>
                  <PencilIcon className="w-5 h-5 mr-2" aria-hidden="true" />
                </div>
                <p
                  className={clsx('text-left mt-2 ', {
                    'text-white': active,
                    'text-gray-600': !active,
                  })}
                >
                  This content won't be publicly accesible.
                </p>
              </button>
            )}
          </Menu.Item>
        </Menu.Items>
      </Transition>
    </Menu>
  );
};

export const SubmitFormButton: React.FC = () => {
  const { values, isSubmitting, initialValues } = useFormikContext();
  const changedValues = getChangedValues(values as any, initialValues as any);
  const isPublished = (initialValues as any).status === StatusFrontend.PUBLISHED;
  const isDraft = (initialValues as any).status === StatusFrontend.DRAFT;

  // Detect Browser refresh etc.
  useBeforeunload((e) => {
    if (!isEmpty(changedValues)) {
      e.preventDefault();
    }
  });

  usePrompt('There are some unsaved changes. Do you want to leave?', !isEmpty(changedValues));

  return (
    <div
      className={clsx('flex divide-x', {
        'divide-gray-400': isPublished,
        'divide-gray-300': isDraft,
      })}
    >
      <button
        type="submit"
        title={isEmpty(changedValues) ? 'No value changed' : ''}
        disabled={isSubmitting || isEmpty(changedValues)}
        className={clsx(
          'disabled:cursor-not-allowed focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 px-4 py-2 inline-flex items-center leading-4 text-white rounded-l-md text-sm font-medium',
          {
            'bg-green-700 focus-visible:ring-green-700 disabled:bg-opacity-70': isPublished,

            'bg-amber-500 focus-visible:ring-amber-700 disabled:bg-opacity-75': isDraft,
          }
        )}
      >
        {isSubmitting ? (
          <>
            <Spinner />
            <span>Submitting...</span>
          </>
        ) : (
          <>
            {isDraft ? (
              <PencilIcon className="w-5 h-5 mr-2" />
            ) : (
              <CheckIcon className="w-5 h-5 mr-2" />
            )}
            <span>Save {isDraft && ' draft'}</span>
          </>
        )}
      </button>
      <SplitButton />
    </div>
  );
};

const Edit = () => {
  const { spaceName, resourceName, resourceId } = useParams();
  const navigate = useNavigate();

  const SpaceKey = `${capitalize(spaceName)}Documents`;
  const itemKey = `${capitalize(resourceName)}Document`;
  const document = (SpaceAPI as any)[SpaceKey][itemKey];
  const resource = useQuery(document, {
    variables: {
      id: resourceId,
    },
  });

  const fields = useQuery(getResourceFieldsDocument, {
    variables: {
      name: `Create${capitalize(resourceName)}DTO`, // we need create DTO so it marks required fields with asterisk
    },
  });

  const errors = resource.error || fields.error;
  const loading = resource.loading || fields.loading;

  const updateKey = `UpdateOne${capitalize(resourceName)}Document`;
  const updateMutation = (SpaceAPI as any)[SpaceKey][updateKey];
  const [updateResource, { error: mutError }] = useMutation(updateMutation);

  if (loading) return <div>Loading ...</div>;
  if (errors) return <pre>{JSON.stringify(errors, null, 2)}</pre>;

  const inputFields = fields.data.__type.inputFields;

  const initialValues: any = {};
  const validationSchema: any = {};

  inputFields.forEach((field: any) => {
    const fieldType = getFieldType(field);
    if (validationYupMap[fieldType]) {
      validationSchema[field.name] = composeFieldValidation(field);
    }

    if (fieldType === 'DateTime') {
      initialValues[field.name] = dayjs(resource.data[`${resourceName}`][field.name]).format(
        'YYYY-MM-DD'
      );
    } else if (fieldType === 'String') {
      initialValues[field.name] = resource.data[`${resourceName}`][field.name] ?? '';
    } else {
      initialValues[field.name] = resource.data[`${resourceName}`][field.name];
    }
  });

  return (
    <div>
      <Formik
        initialValues={initialValues}
        enableReinitialize={true}
        validationSchema={Yup.object().shape(validationSchema)}
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(true);
          await updateResource({
            variables: { input: { id: resourceId, update: values } },
          }).finally(() => {
            setSubmitting(false);
          });
        }}
      >
        {({ errors, touched }) => (
          <Form className="flex flex-col items-start">
            <div className="flex items-center justify-between w-full px-8 h-24 border-b">
              <div className="flex items-center gap-x-4">
                <button
                  className="p-2 border shadow-sm rounded-md hover:bg-gray-50"
                  type="button"
                  onClick={() => navigate(`/${spaceName}/${resourceName}`)}
                >
                  <ArrowLeftIcon className="w-5 h-5 text-gray-400" />
                </button>
                <h2
                  className="font-semibold text-2xl"
                  title={resource.data[`${resourceName}`]['title']}
                >
                  {resource.data[`${resourceName}`]['title']}
                </h2>
              </div>
              <div className="flex gap-x-8 items-center text-sm">
                <span className="text-gray-500 mr-2">
                  Status:{' '}
                  <span
                    className={clsx({
                      'text-green-600':
                        resource.data[`${resourceName}`]['status'] === StatusFrontend.PUBLISHED,
                      'text-amber-500':
                        resource.data[`${resourceName}`]['status'] === StatusFrontend.DRAFT,
                    })}
                  >
                    {capitalize(resource.data[`${resourceName}`]['status'])}
                  </span>
                </span>
                <SubmitFormButton />
              </div>
            </div>
            <div className="bg-gray-50 w-full h-[calc(100vh-6rem)] overflow-auto">
              <div className="p-8 space-y-8">
                {mutError && <pre>{JSON.stringify(mutError, null, 2)}</pre>}
                {inputFields.map((field: any) => {
                  const fieldType = getFieldType(field);
                  const inputType: React.ReactElement = inputTypeMap[fieldType];
                  if (excludedFieldTypes.includes(fieldType)) {
                    return null; // skip excluded fields
                  }

                  const fieldWithProps = React.cloneElement(inputType, {
                    className: 'border rounded-md px-3 py-2',
                    name: field.name,
                  });

                  return (
                    <div key={field.name} className="flex flex-col">
                      <label
                        className="mb-2 block text-sm font-medium text-gray-700"
                        htmlFor={field.name}
                      >
                        {capitalize(field.name)}
                        {isFieldRequired(field) && <span className="text-red-500">&nbsp;*</span>}
                      </label>
                      {fieldWithProps}
                      {touched[field.name] && errors[field.name] && (
                        <div className="mt-2 text-sm text-red-500">{errors[field.name]}</div>
                      )}
                    </div>
                  );
                })}
              </div>
              <MetaData
                id={resourceId}
                created_at={resource.data[`${resourceName}`]['created_at']}
                updated_at={resource.data[`${resourceName}`]['updated_at']}
              />
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default Edit;
