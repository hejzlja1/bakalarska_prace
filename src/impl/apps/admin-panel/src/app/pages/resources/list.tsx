import { DocumentNode } from '@apollo/client';
import { SpaceAPI } from '@bistudio-cms/data-access';
import { useParams } from 'react-router-dom';
import { capitalize } from 'lodash';
import Header from '../../components/resource/Header';
import Datalist from '../../components/resource/Datalist';

const List = () => {
  const { spaceName, resourceName } = useParams();
  const SpaceKey = `${capitalize(spaceName)}Documents`;
  const ResourceKey = `${capitalize(resourceName)}sDocument`;
  const document: DocumentNode = (SpaceAPI as any)[SpaceKey][ResourceKey];

  return (
    <div className="border-r">
      <Header />
      <div className="h-[calc(100vh-6rem)] overflow-auto pb-8">
        <Datalist document={document} />
      </div>
    </div>
  );
};

export default List;
