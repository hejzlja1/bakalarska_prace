import { capitalize } from 'lodash';
import { Link } from 'react-router-dom';
import { clientsUris } from '@bistudio-cms/data-access';

const Spaces = () => {
  const linksRender = Object.entries(clientsUris).map(([key, { url }]) => (
    <li key={url}>
      <Link
        to={key}
        className="bg-white aspect-square border rounded-md p-8 w-full grid place-items-center no-underline text-black font-medium shadow-sm"
      >
        {/* <Logo className="w-10 h-10 mb-1" /> */}
        {capitalize(key)}
      </Link>
    </li>
  ));

  return (
    <div className="grid place-items-center min-h-screen">
      <div>
        <h1 className="text-center mb-4 text-xl font-semibold">Content workspaces</h1>
        <p className="text-center mb-8 text-gray-500">
          Choose workspace from below to start managing content.
        </p>
        <ul className="grid grid-cols-4 gap-8">{linksRender}</ul>
      </div>
    </div>
  );
};

export default Spaces;
