import { CollectionIcon } from '@heroicons/react/outline';

const SpaceDetail = () => {
  return (
    <div className="h-full grid place-items-center">
      <div className="flex flex-col items-center space-y-2">
        <CollectionIcon className="h-10 w-10" />
        <h1 className="text-center font-medium text-xl">No collection selected.</h1>
        <p className="text-center text-gray-500">
          Choose collection on the left side to manage content.
        </p>
      </div>
    </div>
  );
};

export default SpaceDetail;
