import React from 'react';
import { ReactComponent as MSSymbol } from '../../assets/svg/ms_symbol.svg';

const Login = () => {
  return (
    <div className="grid place-items-center min-h-screen">
      <div className="flex flex-col items-center">
        <h1 className="text-center mb-4 text-xl font-semibold">Sign in to your account</h1>
        <p className="text-center mb-8 text-gray-500">
          If you don't have account yet, ask your manager or IT support to provide it to you.
        </p>
        <button className="inline-flex items-center font-medium p-4 gap-x-2 bg-gray-700 text-gray-50">
          <MSSymbol />
          <span>Sign in with Microsoft</span>
        </button>
      </div>
    </div>
  );
};

export default Login;
