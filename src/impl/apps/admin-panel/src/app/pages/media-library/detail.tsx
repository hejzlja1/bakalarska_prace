import { Spinner } from '@bistudio-cms/ui';
import axios from 'axios';
import clsx from 'clsx';
import { useCallback, useRef, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import update from 'immutability-helper';
import Info from '../../components/media-library/Info';
import Actions from '../../components/media-library/Actions';
import ImageAltForm from '../../components/media-library/ImageAltForm';
import { useOneMedia, useFileSize, useAllMedia, clientsUris } from '@bistudio-cms/data-access';

const MediaDetail = () => {
  const { spaceName, mediaId } = useParams();
  const [loading, setLoading] = useState<boolean>(false);
  const navigate = useNavigate();
  const { file, isLoading, isError, mutate: mutateOne } = useOneMedia();
  const imageRef = useRef<HTMLImageElement | null>(null);
  const { fileSize, isLoading: fileSizeLoading, isError: fileSizeError } = useFileSize(file);
  const { mutate } = useAllMedia();

  const handleRemove = useCallback(async () => {
    const ok = window.confirm('Are you sure you want to delete this item?');
    if (ok) {
      if (!spaceName || !mediaId) return;
      const clientURI = clientsUris[spaceName];
      const fullUri = `${clientURI.url}/media-library/${mediaId}`;
      try {
        setLoading(true);
        await axios.delete(fullUri);
      } catch (error) {
        console.log(error);
      } finally {
        setLoading(false);
        mutate((data) => {
          return update(data, () => data?.filter((item) => item._id !== mediaId));
        });
        navigate(`/${spaceName}/media-library`);
      }
    }
  }, [spaceName, mediaId, navigate, mutate]);

  if (isLoading) return <div>Loading...</div>;
  if (isError) {
    return <div>{JSON.stringify(isError, null, 2)}</div>;
  }
  if (!file) return null;

  return (
    <div className="border-l max-h-screen overflow-y-auto bg-white p-8 flex flex-col">
      <img
        ref={imageRef}
        className="rounded-md aspect-[3/2] object-cover"
        src={file.url}
        alt={file.alt}
      />
      <div className="mt-4">
        <div className="font-medium text-lg">{file.key.slice(37)}</div>
        <div className="text-gray-500 text-sm font-medium">
          {fileSizeLoading && <Spinner />}
          {fileSizeError && <div>{JSON.stringify(fileSizeError, null, 2)}</div>}
          <span className={clsx({ invisible: !fileSize })}>
            {fileSize && Math.round(fileSize / 1024 + Number.EPSILON * 100) / 100} KB
          </span>
        </div>
      </div>
      <ImageAltForm alt={file.alt} mutate={mutateOne} />
      <Info file={file} imageRef={imageRef} />
      <Actions file={file} handleRemove={handleRemove} loading={loading} />
    </div>
  );
};

export default MediaDetail;
