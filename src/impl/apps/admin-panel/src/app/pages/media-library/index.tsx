import { Button } from '@bistudio-cms/ui';
import { PlusIcon } from '@heroicons/react/solid';
import clsx from 'clsx';
import { useCallback, useRef, useState } from 'react';
import { Link, Outlet } from 'react-router-dom';
import { useCheckWithIdMedia } from '../../lib/checkWithId';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { IPublicFile } from '@bistudio-cms/data';
import update from 'immutability-helper';
import LocalImage from '../../components/media-library/LocalImage';
import Dropzone from '../../components/media-library/Dropzone';
import { useAllMedia, clientsUris } from '@bistudio-cms/data-access';
import MediaEmptyState from '../../components/media-library/MediaEmptyState';

type FileUploadProgress = {
  file: File;
  progress: number;
};

const MediaLibrary = () => {
  const isDetail = useCheckWithIdMedia();
  const { spaceName } = useParams();
  const [beingUploaded, setBeingUploaded] = useState<FileUploadProgress[]>([]);
  const { files, isLoading, isError, mutate } = useAllMedia();

  const inputFile = useRef<HTMLInputElement>(null);

  const handleUpload = useCallback(
    async (fileList: FileList) => {
      if (!spaceName) return;
      const newFiles: FileUploadProgress[] = Object.entries(fileList).map(([_, item]) => ({
        file: item,
        progress: 0,
      }));
      setBeingUploaded(newFiles);
      try {
        Object.values(fileList).map(async (file: File) => {
          const clientURI = clientsUris[spaceName];
          const fullUri = `${clientURI.url}/media-library/upload`;
          const formData = new FormData();
          formData.append('file', file);
          const res = await axios.post(fullUri, formData, {
            headers: { 'Content-Type': 'multipart/form-data' },
            onUploadProgress: (progressEvent) => {
              const percentCompleted = Math.round(
                (progressEvent.loaded * 100) / progressEvent.total
              );
              setBeingUploaded((prevState) => {
                const fileIndex = prevState.findIndex((item) => {
                  return item.file.name === file.name;
                });

                const newState = update(prevState, {
                  [fileIndex]: { progress: { $set: percentCompleted } },
                });
                return newState;
              });
            },
          });

          const data: IPublicFile = res.data;
          if (files) {
            mutate([data, ...files]);
          }
          setBeingUploaded((prevState) =>
            update(prevState, () => prevState?.filter((item) => item.file.name !== file.name))
          );
        });
      } catch (err) {
        console.log(err);
      }
    },
    [spaceName, mutate, files]
  );

  const handleFiles = useCallback(
    async (e) => {
      if (!spaceName) return;
      const fileList: FileList = e.target.files;
      await handleUpload(fileList);
      if (inputFile.current) {
        inputFile.current.value = '';
      }
    },
    [spaceName, handleUpload]
  );

  if (isLoading) return <div>Loading...</div>;
  if (isError) {
    return <div>{JSON.stringify(isError, null, 2)}</div>;
  }

  if (!files) return null;

  return (
    <div
      className={clsx('grid', {
        'grid-cols-[2fr,1fr]': isDetail,
      })}
    >
      <div className="bg-gray-50 h-screen overflow-y-auto">
        <div className=" flex items-center justify-between pt-8 px-8 h-20">
          <h1 className="font-bold text-2xl">Media library</h1>
          <input
            onChange={handleFiles}
            ref={inputFile}
            className="hidden"
            type="file"
            id="files"
            name="files"
            multiple
          />
          <div className="flex gap-x-4">
            {/* <Button>Filter</Button> */}
            {/* <Button>Bulk Select...</Button> */}
            <Button onClick={() => inputFile.current?.click()} variant="primary">
              <PlusIcon className="w-4 h-4 text-white mr-1" />
              Add media
            </Button>
          </div>
        </div>
        {files.length === 0 && beingUploaded.length === 0 && (
          <div className="h-[calc(100vh-6rem)] grid place-items-center">
            <MediaEmptyState />
          </div>
        )}
        {(files.length > 0 || beingUploaded.length > 0) && (
          <section className="relative p-8 grid grid-cols-4 gap-6">
            {/* Files being uploaded to server */}
            {beingUploaded.map((item, index) => (
              <div key={index}>
                <div className="relative aspect-[3/2]">
                  <LocalImage file={item.file} />
                  <div className="absolute inset-0 bg-black/60 w-full h-full grid place-items-center rounded-md p-4">
                    <div className="w-full">
                      <div className="flex justify-center mb-1">
                        <span className="text-sm font-medium text-blue-700 dark:text-white">
                          {item.progress}%
                        </span>
                      </div>
                      <div className="w-full bg-gray-600 rounded-full h-1.5">
                        <div
                          className="bg-green-600 h-1.5 rounded-full transition-all"
                          style={{ width: `${item.progress}%` }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div title={item.file.name} className="font-medium truncate">
                  {item.file.name}
                </div>
              </div>
            ))}
            {/* Files loaded from server */}
            {files.map((item, index) => (
              <Link className="group focus:outline-none" to={`${item._id}`} key={item._id}>
                <img
                  className="mb-2 aspect-[3/2] object-cover object-center rounded-lg group-focus-within:ring-2 group-focus-within:ring-offset-2 group-focus-within:ring-green-600"
                  src={`${item.url}?random=${index}`}
                  alt=""
                />
                <div title={item.key.slice(37)} className="font-medium truncate">
                  {item.key.slice(37)}
                </div>
              </Link>
            ))}
          </section>
        )}
        <Dropzone handleUpload={handleUpload} />
      </div>
      <Outlet />
    </div>
  );
};

export default MediaLibrary;
