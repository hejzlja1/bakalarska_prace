import { BrowserRouter, Routes, Route } from 'react-router-dom';
import NotFound from './pages/notFound';
import SpaceDetail from './pages/spaces/detail';
import Spaces from './pages/spaces';
import SpaceDetailLayout from './layout/SpaceDetailLayout';
import Create from './pages/resources/create';
import Edit from './pages/resources/edit';
import ListLayout from './layout/ListLayout';
import { ErrorBoundary } from 'react-error-boundary';
import './lib/wdyr';
import ErrorFallback from './components/ErrorFallback';
import Login from './pages/login';
import MediaLibrary from './pages/media-library';
import MediaDetail from './pages/media-library/detail';
import { Toaster } from 'react-hot-toast';
import { SWRConfig } from 'swr';
import { fetcher } from './lib/fetcher';
import { ErrorToast } from '@bistudio-cms/ui';

export function App() {
  return (
    <ErrorBoundary FallbackComponent={ErrorFallback}>
      <SWRConfig
        value={{
          fetcher,
          onError: (error) => {
            ErrorToast(error.toString());
          },
        }}
      >
        <Toaster />
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<Spaces />} />
            <Route path="login" element={<Login />} />
            <Route path=":spaceName" element={<SpaceDetailLayout />}>
              <Route index element={<SpaceDetail />} />
              <Route path="media-library" element={<MediaLibrary />}>
                <Route path=":mediaId" element={<MediaDetail />} />
              </Route>
              <Route path=":resourceName" element={<ListLayout />}>
                <Route path=":resourceId" element={<Edit />} />
                <Route path="create" element={<Create />} />
              </Route>
            </Route>
            <Route path="*" element={<NotFound />} />
          </Routes>
        </BrowserRouter>
      </SWRConfig>
    </ErrorBoundary>
  );
}

export default App;
