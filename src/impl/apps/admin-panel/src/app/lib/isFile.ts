export function isFile(evt: { dataTransfer: DataTransfer }): boolean {
  const dt = evt.dataTransfer;

  for (let i = 0; i < dt.types.length; i++) {
    if (dt.types[i] === 'Files') {
      return true;
    }
  }
  return false;
}
