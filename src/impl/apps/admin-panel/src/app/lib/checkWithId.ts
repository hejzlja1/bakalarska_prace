import { useMatch, useParams } from 'react-router-dom';

export function useCheckWithIdResource() {
  const { spaceName, resourceName } = useParams();
  const isListView = useMatch(`${spaceName}/${resourceName}/*`);

  return isListView?.params['*'] !== '';
}

export function useCheckWithIdMedia() {
  const { spaceName } = useParams();
  const isListView = useMatch(`${spaceName}/media-library/*`);

  return isListView?.params['*'] !== '';
}
