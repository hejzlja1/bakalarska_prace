export type ResourceItem = {
  __typename: string;
  node: {
    id: string;
    title: string;
    status: string;
    created_at: string;
    updated_at: string;
  };
};

export type ResourceItemTable = ResourceItem['node'];
