import { ApolloClient, from, HttpLink, InMemoryCache } from '@apollo/client';
import { clientsUris } from '@bistudio-cms/data-access';
import { onError } from '@apollo/client/link/error';
import { ErrorToast } from '@bistudio-cms/ui';

const createLink = (spaceName: string) => {
  return new HttpLink({
    uri: `${clientsUris[spaceName].url}/graphql`,
  });
};

const getClient = (spaceName: string) => {
  const errorLink = onError((error) => {
    const { graphQLErrors, networkError } = error;
    if (graphQLErrors) {
      for (const err of graphQLErrors) {
        ErrorToast('GraphQL error:', err.message);
      }
    }

    if (networkError) {
      ErrorToast('Network error:', networkError.message);
    }
  });
  if (clientsUris[spaceName])
    return new ApolloClient({
      link: from([errorLink, createLink(spaceName)]),
      cache: new InMemoryCache(),
    });
  else {
    ErrorToast('Error: ', "Client for this space doesn't exist.");
    throw new Error("Client for this space doesn't exist.");
  }
};

export default getClient;
