import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/app.module';
import { PositionSeeder } from './app/positions/position.seeder';
import { PositionsModule } from './app/positions/positions.module';
import { PostSeeder } from './app/posts/post.seeder';
import { PostsModule } from './app/posts/posts.module';

async function bootstrap() {
  const app = await NestFactory.createApplicationContext(AppModule);
  const postsSeeder = app.select(PostsModule).get(PostSeeder, { strict: true });
  const positionsSeeder = app.select(PositionsModule).get(PositionSeeder, { strict: true });
  await postsSeeder.seed();
  await positionsSeeder.seed();
  await app.close();
}
bootstrap();
