import { Module, OnModuleInit } from '@nestjs/common';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { NestjsQueryMongooseModule } from '@nestjs-query/query-mongoose';
import { CreatePositionDTO } from './createPosition.dto';
import { Position, PositionSchema } from './position.entity';
import { ReadPositionDTO } from './readPosition.dto';
import { UpdatePositionDTO } from './updatePosition.dto';
import { PositionSeeder } from './position.seeder';
import { EntitiesService, MetadataModule } from '@bistudio-cms/core';
import { environment } from '../../environments/environment';

@Module({
  providers: [PositionSeeder],
  imports: [
    MetadataModule,
    NestjsQueryGraphQLModule.forFeature({
      imports: [
        NestjsQueryMongooseModule.forFeature([
          {
            document: Position,
            name: `${environment.entityPrefix}_${Position.name}`,
            schema: PositionSchema,
          },
        ]),
      ],
      resolvers: [
        {
          DTOClass: ReadPositionDTO,
          EntityClass: Position,
          CreateDTOClass: CreatePositionDTO,
          UpdateDTOClass: UpdatePositionDTO,
        },
      ],
    }),
  ],
})
export class PositionsModule implements OnModuleInit {
  constructor(private entitiesService: EntitiesService) {}
  onModuleInit() {
    this.entitiesService.registerEntity(Position);
  }
}
