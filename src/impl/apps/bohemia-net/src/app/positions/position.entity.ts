import { BaseEntity } from '@bistudio-cms/core';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema()
export class Position extends BaseEntity {
  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  excerpt: string;

  @Prop({ type: mongoose.Schema.Types.Mixed })
  content?: object;

  @Prop({ required: true })
  project: string;

  @Prop({ required: true })
  diciplines: string;

  @Prop({ required: true })
  location: string;
}

export const PositionSchema = SchemaFactory.createForClass(Position);
