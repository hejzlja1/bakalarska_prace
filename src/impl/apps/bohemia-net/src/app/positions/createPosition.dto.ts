import { CreateBaseDTO } from '@bistudio-cms/core';
import { InputType, Field } from '@nestjs/graphql';
import { GraphQLJSONObject } from 'graphql-type-json';
import { IsNotEmpty } from 'class-validator';

@InputType()
export class CreatePositionDTO extends CreateBaseDTO {
  @Field()
  @IsNotEmpty()
  title: string;

  @Field()
  @IsNotEmpty()
  excerpt: string;

  @Field(() => GraphQLJSONObject, { nullable: true })
  content?: object;

  @Field()
  @IsNotEmpty()
  project: string;

  @Field()
  @IsNotEmpty()
  diciplines: string;

  @Field()
  @IsNotEmpty()
  location: string;
}
