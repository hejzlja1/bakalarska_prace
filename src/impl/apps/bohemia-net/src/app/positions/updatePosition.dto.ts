import { CreatePositionDTO } from './createPosition.dto';
import { InputType, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdatePositionDTO extends PartialType(CreatePositionDTO) {
  id: number;
}
