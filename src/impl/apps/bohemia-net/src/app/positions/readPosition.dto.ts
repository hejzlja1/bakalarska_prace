import { BaseDTO } from '@bistudio-cms/core';
import { Field, ObjectType } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';
import { GraphQLJSONObject } from 'graphql-type-json';

@ObjectType('Position')
export class ReadPositionDTO extends BaseDTO {
  @FilterableField()
  title: string;

  @FilterableField()
  excerpt: string;

  @Field(() => GraphQLJSONObject, { nullable: true })
  content?: object;

  @FilterableField()
  project: string;

  @FilterableField()
  diciplines: string;

  @FilterableField()
  location: string;
}
