import { faker } from '@faker-js/faker';
import { InjectQueryService, QueryService } from '@nestjs-query/core';
import { Injectable } from '@nestjs/common';
import { Status } from '@bistudio-cms/data';
import { Position } from './position.entity';
import { CreatePositionDTO } from './createPosition.dto';

@Injectable()
export class PositionSeeder {
  constructor(@InjectQueryService(Position) readonly service: QueryService<Position>) {}
  async seed(): Promise<void> {
    // will remove table for every startup, for testing now
    //this.service.query(`DELETE FROM ${this.repository.metadata.tableName}`);
    this.service.deleteMany({});
    const positions = Array.from({ length: 10 }, () => {
      const position = new CreatePositionDTO();
      position.title = faker.lorem.words();
      position.excerpt = faker.lorem.paragraph();
      position.diciplines = faker.lorem.words();
      position.project = faker.lorem.word();
      position.location = faker.address.city();
      position.status = faker.random.arrayElement(Object.values(Status));
      return position;
    });

    // Insert into the database.
    await this.service.createMany(positions);
  }
}
