import { Module, OnModuleInit } from '@nestjs/common';
import { NestjsQueryGraphQLModule } from '@nestjs-query/query-graphql';
import { CreatePostDTO } from './createPost.dto';
import { Post, PostSchema } from './post.entity';
import { ReadPostDTO } from './readPost.dto';
import { UpdatePostDTO } from './updatePost.dto';
import { PostSeeder } from './post.seeder';
import { NestjsQueryMongooseModule } from '@nestjs-query/query-mongoose';
import { EntitiesService, MetadataModule } from '@bistudio-cms/core';
import { environment } from '../../environments/environment';

@Module({
  providers: [PostSeeder],
  imports: [
    MetadataModule,
    NestjsQueryGraphQLModule.forFeature({
      imports: [
        NestjsQueryMongooseModule.forFeature([
          {
            document: Post,
            name: `${environment.entityPrefix}_${Post.name}`,
            schema: PostSchema,
          },
        ]),
      ],
      resolvers: [
        {
          DTOClass: ReadPostDTO,
          EntityClass: Post,
          CreateDTOClass: CreatePostDTO,
          UpdateDTOClass: UpdatePostDTO,
        },
      ],
    }),
  ],
})
export class PostsModule implements OnModuleInit {
  constructor(private entitiesService: EntitiesService) {}
  onModuleInit() {
    this.entitiesService.registerEntity(Post);
  }
}
