import { BaseEntity } from '@bistudio-cms/core';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema()
export class Post extends BaseEntity {
  @Prop({ type: mongoose.Schema.Types.Mixed })
  content?: object;

  @Prop()
  category?: string;
}

export const PostSchema = SchemaFactory.createForClass(Post);
