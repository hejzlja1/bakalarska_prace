import { CreatePostDTO } from './createPost.dto';
import { InputType, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdatePostDTO extends PartialType(CreatePostDTO) {
  id: number;
}
