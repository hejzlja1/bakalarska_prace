import { BaseDTO } from '@bistudio-cms/core';
import { Field, ObjectType } from '@nestjs/graphql';
import { FilterableField } from '@nestjs-query/query-graphql';
import { GraphQLJSONObject } from 'graphql-type-json';

@ObjectType('Post')
export class ReadPostDTO extends BaseDTO {
  @FilterableField()
  title: string;

  @Field(() => GraphQLJSONObject, { nullable: true })
  content?: object;

  @FilterableField({ nullable: true })
  category?: string;
}
