import { faker } from '@faker-js/faker';
import { InjectQueryService, QueryService } from '@nestjs-query/core';
import { Injectable } from '@nestjs/common';
import { Status } from '@bistudio-cms/data';
import { Post } from './post.entity';
import { CreatePostDTO } from './createPost.dto';

@Injectable()
export class PostSeeder {
  constructor(@InjectQueryService(Post) readonly service: QueryService<Post>) {}
  async seed(): Promise<void> {
    // will remove table for every startup, for testing now
    //this.service.query(`DELETE FROM ${this.repository.metadata.tableName}`);
    this.service.deleteMany({});
    const posts = Array.from({ length: 10 }, () => {
      const post = new CreatePostDTO();
      post.title = faker.lorem.words();
      post.category = faker.datatype.boolean() ? faker.lorem.word() : null;
      post.status = faker.random.arrayElement(Object.values(Status));
      return post;
    });

    // Insert into the database.
    await this.service.createMany(posts);
  }
}
