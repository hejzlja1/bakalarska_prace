import { CreateBaseDTO } from '@bistudio-cms/core';
import { InputType, Field } from '@nestjs/graphql';
import { IsNotEmpty } from 'class-validator';
import { GraphQLJSONObject } from 'graphql-type-json';

@InputType()
export class CreatePostDTO extends CreateBaseDTO {
  @Field()
  @IsNotEmpty()
  title: string;

  @Field(() => GraphQLJSONObject, { nullable: true })
  content?: object;

  @Field({ nullable: true })
  category?: string;
}
