# BISTUDIO - CMS

## Install

`pnpm install`

## Before running apps

### Database

Download and run MongoDB in container using [Docker](https://hub.docker.com/_/mongo).

### Environment variables

Create .env file in the project root and add variables based on .env.example

### Seeding

`pnpm nx run-many --target=seed --all`

### Generate operations based on GraphQL schema for the admin panel.

`pnpm nx run-many --target=generate-operations --all`

## Run all apps

`pnpm nx run-many --target=serve --all --maxParallel=10`

`maxParallel is 3 by default, if you want to run more then 3 projects at once, set it to higher value.`

## Run individual project

`pnpm nx run bohemia-net:serve`

## Run only selection of apps

`pnpm nx run-many --target=serve --projects=admin-panel,bohemia-net`

---

This project was generated using [Nx](https://nx.dev).

## Generate an application

Run `pnpm nx g @nrwl/nest:app my-app` to generate an application.

> You can use any of the plugins above to generate applications as well.

When using Nx, you can create multiple applications and libraries in the same workspace.

## Generate a library

Run `pnpm nx g @nrwl/nest:lib my-lib` to generate a library.

> You can also use any of the plugins above to generate libraries as well.

Libraries are shareable across libraries and applications. They can be imported from `@bistudio-cms/mylib`.

## Development server

Run `pnpm nx serve my-app` for a dev server. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `pnpm nx g @nrwl/react:component my-component --project=my-app` to generate a new component.

## Build

Run `pnpm nx build my-app` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `pnpm nx test my-app` to execute the unit tests via [Jest](https://jestjs.io).

Run `pnpm nx affected:test` to execute the unit tests affected by a change.

## Running end-to-end tests

Run `pnpm nx e2e my-app` to execute the end-to-end tests via [Cypress](https://www.cypress.io).

Run `pnpm nx affected:e2e` to execute the end-to-end tests affected by a change.

## Understand your workspace

Run `pnpm nx graph` to see a diagram of the dependencies of your projects.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.

## ☁ Nx Cloud

### Distributed Computation Caching & Distributed Task Execution

<p style="text-align: center;"><img src="https://raw.githubusercontent.com/nrwl/nx/master/images/nx-cloud-card.png"></p>

Nx Cloud pairs with Nx in order to enable you to build and test code more rapidly, by up to 10 times. Even teams that are new to Nx can connect to Nx Cloud and start saving time instantly.

Teams using Nx gain the advantage of building full-stack applications with their preferred framework alongside Nx’s advanced code generation and project dependency graph, plus a unified experience for both frontend and backend developers.

Visit [Nx Cloud](https://nx.app/) to learn more.
